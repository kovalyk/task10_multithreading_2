package com.epam.model.task1;

public class Task1Impl {
    Task1 app = new Task1();

    public void executeTask1() {
        Thread thread1 = new Thread(() -> app.method1());
        Thread thread2 = new Thread(() -> app.method2());
        Thread thread3 = new Thread(() -> app.method3());

        thread1.start();
        thread2.start();
        thread3.start();

        try {
            thread1.join();
            thread2.join();
            thread3.join();
        } catch (InterruptedException e) {
            System.out.println("Interrupt");
        }
    }
}

