package com.epam.model.task1;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Task1 {
    Lock lock = new ReentrantLock();

    public void method1() {
        lock.lock();
        try {
            for (int i = 0; i < 1000; i++) {
                System.out.println(i + " this is method 1");
            }
        } finally {
            lock.unlock();
        }
    }

    public void method2() {
        lock.lock();
        try {
            for (int i = 0; i < 1000; i++) {
                System.out.println(i + " this is method 2");
            }
        } finally {
            lock.unlock();
        }
    }

    public void method3() {
        lock.lock();
        try {
            for (int i = 0; i < 1000; i++) {
                System.out.println(i + " this is method 3");
            }
        } finally {
            lock.unlock();
        }
    }
}
