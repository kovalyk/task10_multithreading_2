package com.epam.view;

import com.epam.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class View {
    public final static String EXIT = "Q";
    public Logger logger = LogManager.getLogger(View.class);
    private Controller controller = new Controller();
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private Scanner input;

    public View() {
        input = new Scanner(System.in);
        menu = new LinkedHashMap<>();
        menu.put("1", "\t1 - Show Ping Pong game");
        menu.put("2", "\t2 - Show BlockingQueue");
        menu.put("3", "\t3 - Show ReadWriteLock");
        menu.put("Q", "\tQ - exit");
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::showTask1);
        methodsMenu.put("2", this::showBlockingQueue);
        methodsMenu.put("3", this::showReadWriteLock);
    }

    private void showTask1() {
        controller.getTask1();
    }

    private void showBlockingQueue() {
        controller.getBlockingQueue();
    }

    private void showReadWriteLock() {
        controller.getReadWriteLock();
    }

    private void outputMenu() {
        logger.warn("\nMENU:");
        for (String str : menu.values()) {
            logger.warn(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            logger.warn("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                logger.error("Invalid input! Please try again.");
            }
        } while (!keyMenu.equals(EXIT));
    }
}
